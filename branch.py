from BasicVector import Vec2
import random

class Branch:

    def __init__(self, size, start, rotation):
        self.size = size
        self.start = start
        self.rotation = rotation

    def more(self):
        branches = [self]
        if self.size > 2:

            new_size = self.size * (random.randrange(7, 9) / 10) + (random.randrange(0, 1))

            end = self.start + Vec2.degreesToVec2(self.rotation) * self.size

            temp = self.rotation - 30 + random.randrange(-25, 25)
            left = Branch(new_size, end, temp)

            temp = self.rotation + 30 + random.randrange(-25, 25)
            right = Branch(new_size, end, temp)

            branches.extend(left.more())
            branches.extend(right.more())

        return branches
