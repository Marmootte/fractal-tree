import pygame
from pygame.locals import *
import time
from BasicVector import Vec2


from branch import Branch
from renderer import *


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    init_branch = Branch(80, Vec2(300, 600), -90)

    branches = init_branch.more()

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    branches = init_branch.more()

        if not pause:
            pass

        if frameCount % 1 == 0:
            render(window, branches)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
