import pygame
from BasicVector import Vec2


def clear(window):
    window.fill((0, 0, 0))


def render(window, branches):
    clear(window)
    for branch in branches:
        if branch.size < 10:
            color = (45, 90, 39)
        else:
            color = (120, 81, 21)

        start = branch.start.getPos()
        end = (branch.start + (Vec2.degreesToVec2(branch.rotation) * branch.size)).getPos()
        width = int(branch.size / 10)
        if width == 0:
            width = 1

        pygame.draw.line(window, color, start, end, width)
